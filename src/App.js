import React from 'react';
import './App.css';
import Navbar from './components/navbar';

function App() {
  return (
    <div className="App">

      {/* <Navbar/> */}
      
      <section className="Renovations">
        <h1>Hey there! Looks like you're a potential connection!</h1>
        <p>This website is currently being renovated at the moment but here are some important links:</p>
        <ul>
          <li><a href="https://drive.google.com/file/d/1WXp6yW6qNsOEtKer604Tvsmnktcga53c/view?usp=sharing">Resume</a></li>
          <li><a href="https://gitlab.com/Snoogle">Portfolio</a></li>
          <li><a href="https://www.instagram.com/snoogle_bunny/">Instagram</a></li>
        </ul>
      </section>

    {/* 
      <section className="connect">
        <img src="" alt="Connect" />
        <p>Connect</p>
      </section>

      <section className="explore">
        <img src="" alt="Explore" />
        <p>Explore</p>
      </section>

      <section className="create">
        <img src="" alt="Create" />
        <p>Create</p>
      </section>

      <footer>

      </footer> */}
      
    </div>

  );
}

export default App;
