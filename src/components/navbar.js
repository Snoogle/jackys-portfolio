import React from 'react'

export default class Navbar extends React.Component {

    constructor(){
        super();
        this.state = {
            isMobile: false,
            easterEggUnlocked: false,
        }
    }

    render(){
        return(
            <div className="Navbar">
                <nav className="Navbar_Items">
                    <div className="Navbar_Link Navbar_Link-brand">Website Title</div>
                    <div className="Navbar_Link Navbar_Link-toggle">
                        <i className="fas fa-bars" onClick={() => {
                            const navs = document.querySelectorAll('.Navbar_Items');
                            navs.forEach(nav => nav.classList.toggle('Navbar_ToggleShow'));
                            document.querySelector('.Navbar_Link-toggle')
                                .addEventListener('click', classToggle);  
                        }}></i>
                    </div>
                    <div className="Navbar_Link">Example</div>
                    <div className="Navbar_Link">Example2</div>
                    <div className="Navbar_Link">Example3</div>
                    <div className="Navbar_Link">Example4</div>
                    <div className="Navbar_Link">Example5</div>
                </nav>
            </div>
        );
    };
}

function classToggle(){
    const navs = document.querySelectorAll('.Navbar_Items');
}

  
